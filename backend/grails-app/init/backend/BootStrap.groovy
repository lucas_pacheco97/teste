package backend
import java.time.LocalDate
import java.time.LocalTime
import java.time.Period

class BootStrap {

    def init = { servletContext ->

        def companies = [
                new Company(name: "Chevrolet", segment: "Vehicles").save(),
                new Company(name: "Fiat", segment: "Vehicles").save(),
                new Company(name: "Toyota", segment: "Vehicles").save()
        ]
        for (comp in companies){

            def initialDate = LocalDate.now() - Period.ofDays(30)
            def today = LocalDate.now()
            def now = LocalTime.now()
            def start = new Date(initialDate.year - 1900, initialDate.month.value - 1, initialDate.dayOfMonth, 10, 0, 0)
            def end = new Date(today.year - 1900, today.month.value -1, today.dayOfMonth, now.hour, now.minute, 0)

//            println "Start: " + start
//            println "End: " + end
//            println "Bool: " + (start < end)

            while (start < end){
                if(start.getHours()>=10 && start.getHours()<=17 || (start.getHours()==18 && start.getMinutes()==0)){
                    new Stock(company: comp, price: Math.random(), priceDate: start).save()
                }
                start.setMinutes(start.getMinutes() + 1)

            }
//            for (i in 0..<(24*60*30)+1) {
//
//                Date date = new Date()
//                date.setMinutes(start.getMinutes() + i)
//                println date
//
//            }



//            Date data = new Date(2022-1900, 3, 10, 23, 58,0)
//            data.setMinutes(data.getMinutes() + 3)
//            println data

//            for (day in 1..<31){
//                for (hour in 10..<19) {
//                    for (minutes in 0..<60) {
//                        if (hour == 18) {
//                            date = new Date(initialDate.year - 1900, initialDate.month.value - 1, initialDate.dayOfMonth + day, hour, minutes, 0)
//                            println date
//                            break
//                        }
//                        Date date = new Date(initialDate.year - 1900, initialDate.month.value - 1, initialDate.dayOfMonth + day, hour, minutes, 0)
//                        //println date
//
////                        println date.month + " - " + today.month.value -1
//                        //println date.date + " - " + today.dayOfMonth
////                        println hour + " - " +  now.hour
////                        println minutes + " - " + now.minute
////                        println "---------------"
//                        if (date.month == today.month.value - 1 && date.date == today.dayOfMonth && hour >= now.hour && minutes >= now.minute) {
//                            break
//                        }
//                        println date
//                    }
//                }
//            }
//            println now.minute

            //def time = LocalTime.now()
//            System.out.println("LocalDate : "+ initialDate);
            //System.out.println("LocalTime : "+ time);
            //new Stock(company: comp, price: 12.00, priceDate: date).save()
        }




    }
    def destroy = {
    }
}
