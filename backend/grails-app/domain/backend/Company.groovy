package backend

class Company {

    String name
    String segment

    static constraints = {
        name inList: ['Chevrolet', 'Toyota', 'Fiat']
        segment maxSize: 255
    }
}
