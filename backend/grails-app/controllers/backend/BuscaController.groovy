package backend

class BuscaController {
    
    def buscaService

    def index() {
        def stocks = Stock.list()
        render(model: [stocks: stocks], view: "index")
    }

    def search(){
        println("Passou pelo controller")
        //recebe os ids dos produtos
        def company = params.company
        //recebe as quantidades digitadas pelo usuário
        def hour = params.hours
        //throw("Passou pelo controller")

        //chama o service para salvar a venda (pedido)
        buscaService.getStocks(params.company, params.hours)
        println("Passou pelo controller")


    }
}
